package test2_tran;
/**
 * A class to store information about the pay of an unionized employee.
 * @author Celine 1938648
 *
 */
public class UnionizedHourlyEmployee extends HourlyEmployee {
	/**
	 * The pension contributed to an unionized worker. 
	 */
	private double pensionContribution;
	
	/**
	 * A constructor that initializes the UnionizedHourlyEmployee object
	 * @param hoursAWeek The hours worked per week.
	 * @param hourlyPay The pay rate per hour.
	 * @param pensionContribution The pension contributed to an unionized worker in question.
	 */
	public UnionizedHourlyEmployee(double hoursAWeek,double hourlyPay,double pensionContribution) {
		super(hourlyPay,hoursAWeek);
		this.pensionContribution=pensionContribution;
	}
	
	/**
	 * @return  The pension contributed to an unionized worker in question.  
	 */
	public double getpensionContribution() {
		return pensionContribution;
	}
	
	/**
	 * An overrided method that gets the yearly pay of an unionized employee.
	 * @return the yearly pay of the employee in question.
	 */
	@Override
	public double getYearlyPay() {
		double yearlyPay= (getHoursAWeek()*getHourlyPay())*52+pensionContribution;
		return yearlyPay;
	}

}
