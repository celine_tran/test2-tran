package test2_tran;
/**
 * An interface for getting yearly pay.
 * @author Celine 1938648
 *
 */
public interface Employee {
	/**
	 * The yearly pay of an employee.
	 */
	double getYearlyPay();
}
