package test2_tran;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	

	/**
	 * Override the equals() method so that two Planet objects are considered equal if and only if both their name 
		and order fields are identical
	 * @author Celine
	 * @return True of false depending if name and order match or not.
	 */
public boolean equals(Object o) {
		if (! (o instanceof Planet)) { 
			return false;
			}
		Planet aPlanet = (Planet)o;
		if(this.name==aPlanet.getName() && this.order==aPlanet.getOrder()) {
			return true;
		}
		else {
		return false;
	}
}

/**
 * Override the hashCode() method in such a way that Planet objects can be use within the various collection classes 
that we discussed. You should follow the best practices we discussed to do this.
 * @author Celine
 * @return The combination of the Strings 
 */
		public int hashCode() {
			String combination = this.order + this.name;
			return combination.hashCode();
		}	

		/**
		 * Modify the Planet class so that it is compatible with the method Arrays.sort(). After calling Arrays.sort()
 			on a Planet[] planets, the array planets should be sorted first by name (increasing order) and in the case that 
 			the names are equal, then by planetarySystemName (decreasing order)
		 * @return an int indicating if its smaller than or bigger than.
		 * @author Celine
		 */
	public int compareTo(Planet aPlanet) {
		int diffElements = this.name.compareTo(aPlanet.name);
		if(diffElements==0) {
			return -1*this.planetarySystemName.compareTo(aPlanet.planetarySystemName);	
		}
		return diffElements;
	}
	
}

















