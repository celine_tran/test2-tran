package test2_tran;
/**
 * A class to store yearly pay of an hourly employee.
 * @author Celine 1938648
 *
 */
public class HourlyEmployee implements Employee{
	/**
	 * The hours worked per week 
	 */
private double hoursAWeek;
/**
 * The pay rate per hour 
 */
private double hourlyPay;

/**
 * A constructor that initializes the HourlyEmployee object.
 * @param hoursAWeek The hours worked per week in question.
 * @param hourlyPay The pay rate per hour in question.
 */
public HourlyEmployee(double hoursAWeek,double hourlyPay) {
	this.hourlyPay=hourlyPay;
	this.hoursAWeek=hoursAWeek;
}
/**
 * An overrided method that gets the yearly pay of an employee.
 * @return the yearly pay of the hourly employee in question.
 */
	@Override
	public double getYearlyPay() {
		double yearlyPay= (getHoursAWeek()*getHourlyPay())*52;
		return yearlyPay;
	}
	/**
	 * @return  The pay rate per hour  
	 */
	public double getHourlyPay() {
		return hourlyPay;
	}
	
	/**
	 * @return  The hours worked per week 
	 */
	public double getHoursAWeek() {
		return hoursAWeek;
	}

}
