package test2_tran;
/**
 * A class to store yearly pay of an employee.
 * @author Celine 1938648
 *
 */
public class SalariedEmployee implements Employee{
	/**
	 * The yearly salary of an employee
	 */
	private double EmpYearSal;

	/**
	 * A constructor that initializes the SalariedEmployee object
	 * @param EmpYearSal The yearly salary of an employee.
	 */
	public SalariedEmployee(double  EmpYearSal) {
		this.EmpYearSal=EmpYearSal;
	}
	
	/**
	 * A method that gets the yearly pay of an employee.
	 * @return The employee salary in question.
	 */
	@Override
	public double getYearlyPay() {
		return this.EmpYearSal;
		
}
}
