package test2_tran;

public class PayrollManagement {	
	/**
	 * A class to declare 5 employees and calculate the total pay of all employee. 
	 * @author Celine 1938648
	 *
	 */
		public static void main(String[] args) {
			//creates an array of 5 employees.
		Employee[] employee=new Employee[5];
		//initializes 5 employees with hardcoded values.
		employee[0]=new HourlyEmployee(10.50,40);
		employee[1]=new UnionizedHourlyEmployee(13.50,35,5000);
		employee[2]=new SalariedEmployee(30500);
		employee[3]=new HourlyEmployee(13.75,43);
		employee[4]=new UnionizedHourlyEmployee(12.18,35,7034);
		//calls the method to get total expenses
		double allEmployeeSal=getTotalExpenses(employee);  
		//prints total expenses
		System.out.println("total expenses: "+allEmployeeSal + "$");
}
		/**
		 *  This method should take as input an Employee[] and return the total
	 		amount of yearly expenses by adding all the yearly salaries of the employees.
		 * @param employees an array of many employees
		 * @return  The total yearly expenses of all employee salaries.  
		 */
	public static double getTotalExpenses(Employee [] employees) {
		double totalYearlyExpenses = 0;
		for(Employee sal : employees) {
			totalYearlyExpenses+=sal.getYearlyPay();
		}
		
		return totalYearlyExpenses;
	}
	

}